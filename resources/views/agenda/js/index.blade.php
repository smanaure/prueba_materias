<script>
    $(function () {
        var agenda_table = $("#agenda_table");

        cargar_datos();

        function cargar_datos() {
            agenda_table.DataTable({
                'ajax': {
                    "url": '{{url("/agenda")}}',
                    "type": "GET",
                    dataSrc: ''
                },
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por pagina",
                    "zeroRecords": "No se encontraron registros",
                    "info": "Pagina _PAGE_ De _PAGES_",
                    "infoEmpty": "Sin registros disponibles",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Inicio",
                        "last":       "Final",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                },
                'columns': [
                    {data: 'nombre_materia'},
                    {data: 'docente'},
                    {data: 'tipoMateria'},
                    {
                        render: function (data, type, row) {
                            var ruta = "{{url('/agenda')}}/" + row.id + "/edit"
                            btn_editar = '<a href="' + ruta + '" class="btn btn-sm btn-success" title="Editar" data-toggle="tooltip"><span class="glyphicon glyphicon-edit"></span></a>';
                            var btn_eliminar = "";
                            btn_eliminar = '<a href="' + row.id + '" class="btn btn-sm btn-danger deletee" title="Eliminar" data-toggle="tooltip"><span class="glyphicon glyphicon-remove"></span></a>';
                            return (btn_editar + ' ' + btn_eliminar);
                        }
                    }
                ]
            });
        }


        $('body').on('click', 'tbody .deletee', function (e) {
            idAgenda = $(this).attr('href');
            token = $("input[name=_token]").val();
            e.preventDefault();
            swal({
                title: '',
                text: '¿Seguro desea eliminar el registro?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: "{{url('/agenda')}}/" + idAgenda,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'json',
                    success: function (respuesta) {
                        if (respuesta.success) {
                            agenda_table.DataTable().ajax.reload();
                            toastr.success(respuesta.message);
                        } else {
                            toastr.error(respuesta.error);
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

        $("#frmNewAgenda").submit(function(e){
            e.preventDefault();
            token = $("input[name=_token]").val();
            var data= new FormData();
            data.append ('nombre_materia', $("input[name=nombre_materia]").val());
            data.append ('docente', $("input[name=docente]").val());
            data.append ('tipoMateria', $("#tipoMateria").val());
            $.ajax({
                url: "{{url('/agenda')}}",
                type: "POST",
                cache: false,
                processData: false,
                contentType: false,
                headers: {'X-CSRF-TOKEN': token},
                data: data,
                success: function (respuesta) {
                    if (respuesta.success) {
                        toastr.success(respuesta.message);
                        $('#frmNewAgenda')[0].reset();
                        agenda_table.DataTable().ajax.reload();
                    }else {
                        toastr.error(respuesta.error);
                    }
                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                }
            });
            
        })

    });
</script>