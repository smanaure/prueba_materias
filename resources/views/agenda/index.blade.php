@extends('layout')
@section('content')
<br>
<br>
<br>
<br>
<br>

<div class="container">
	<div class="row">
		<h2 class="text-center">Crear Nueva Materia</h2>
		<form action="" style="" class="form-inline" id="frmNewAgenda">
			@csrf
			<div class="form-group">
				<label for="nombre_materia">Nombre de Materia</label>
				<input type="text" class="form-control" id="nombre_materia" name="nombre_materia" required>
			</div>
			<div class="form-group">
				<label for="docente">Nombre de Docente</label>
				<input type="text" class="form-control" id="docente" name="docente" required>
			</div>
			<div class="form-group">
				<label for="tipoMateria">Tipo de Materia</label>
				<select class="form-control" id="tipoMateria" name="tipoMateria" required>
				  <option value="Cuantitativa">Cuantitativa</option>
				  <option value="Cualitativa">Cualitativa</option>
				</select>
			</div>
			<button type="submit" class="btn btn-default">Agregar</button>
		</form>
	</div>
	<div class="row">
		<h2 class="text-center">Listado de Materias</h2>
		@include('flash::message')
		<table id="agenda_table">
			<thead>
				<tr>
					<th class="text-center tr-head">Nombre de Materia</th>
					<th class="text-center tr-head">Nombre de Docente</th>
					<th class="text-center tr-head">Tipo de Materia</th>
					<th class="text-center tr-head">Opciones</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
	<br><br>
</div>
<br>
<br>
@include('plugins.datatable')
@include('agenda.js.index')
@endsection
