@extends('layout')
@section('content')
<br>
<br>
<br>
<br>
<br>
<h2 class="text-center">Editar Producto</h2>
<div class="container">
	<form action="{{url('/agenda')}}/{{$agenda->id}}" method='POST'>
		@csrf
		<input type="hidden" name="_method" value="PUT">
		<div class="form-group">
			<label for="nombre_materia">Nombre de Materia</label>
			<input type="text" class="form-control" id="nombre_materia" name="nombre_materia" value="{{$agenda->nombre_materia}}" required>
		</div>
		<div class="form-group">
			<label for="docente">Nombre de Docente</label>
			<input type="text" class="form-control" id="docente" name="docente" value="{{$agenda->docente}}" required>
		</div>
		<div class="form-group">
			<label for="tipoMateria">Tipo de Materia</label>
			<select class="form-control" id="tipoMateria" name="tipoMateria" required>
			  <option @if (($agenda->tipoMateria == "Cuantitativa")) selected @endif value="Cuantitativa">Cuantitativa</option>
			  <option @if (($agenda->tipoMateria == "Cualitativa")) selected @endif value="Cualitativa">Cualitativa</option>
			</select>
		</div>
		<div class="row">
			<div class="col-md-6">
				<a href="{{url('/agenda')}}" class="btn btn-lg btn-block btn-default">Cancelar</a>
			</div>
			<div class="col-md-6">
				<input type="submit" class="btn btn-lg btn-block btn-primary" value="Actualizar">
			</div>
		</div>
	</form>
</div>
<br>
<br>
@endsection
