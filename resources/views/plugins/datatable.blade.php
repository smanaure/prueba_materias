@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css') }}"/>
@endpush
@push('js')
<script type="text/javascript" src="{{ asset('js/datatables.min.js') }}"></script>
@endpush