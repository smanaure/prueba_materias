<?php

namespace App\Http\Controllers;

use App\Agenda;
use Illuminate\Http\Request;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if($request->ajax()){
            $agenda = Agenda::all();
            return response()->json($agenda);
        }else{
            return view('agenda.index',compact('seccion'));
        }            
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        try {
            $agenda = new Agenda();
            $agenda->nombre_materia = $request->nombre_materia;
            $agenda->docente = $request->docente;
            $agenda->tipoMateria = $request->tipoMateria;
            $agenda->save();
            return response()->json(["success" => true, "message" => "Se agregó correctamente"]);
        } catch (Exception $e) {
            return response()->json(["success" => false, "error" => $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function show(Agenda $agenda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agenda = Agenda::findOrFail($id);
        return view('agenda.edit',compact('agenda'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agenda = Agenda::findOrFail($id);
        try {
            $agenda->nombre_materia = $request->nombre_materia;
            $agenda->docente = $request->docente;
            $agenda->tipoMateria = $request->tipoMateria;
            $agenda->save();
            flash('Agenda Actualizada Exitosamente')->success();
            return redirect('/agenda');
        } catch (Exception $e) {
            flash('Error: No se actualizo la agenda')->Error();
            return redirect('/agenda');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agenda = Agenda::findOrFail($id);
        try {
            $agenda->delete();
            return response()->json(["success" => true, "message" => "Se elimino correctamente"]);
        } catch (Exception $e) {
            return response()->json(["success" => false, "error" => $e]);
        }
    }
}
